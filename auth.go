package tarantool

import (
	"crypto/sha1"
	"encoding/base64"
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

// Auth provides authentication Query.
type Auth struct {
	user     string
	password string
	salt     []byte
}

// NewAuth returns a new instance of authentication Query.
func NewAuth(user, password string) *Auth {
	return &Auth{user: user, password: password}
}

// EncodeMsgpack implements msgpack.CustomEncoder interface.
func (a *Auth) EncodeMsgpack(enc *msgpack.Encoder) error {
	salt := make([]byte, base64.StdEncoding.DecodedLen(saltSize))

	n, err := base64.StdEncoding.Decode(salt, a.salt[:saltSize])
	if err != nil {
		return fmt.Errorf("decode base64 salt string: %w", err)
	}

	if err = enc.MapLen(authKeysNum); err != nil {
		return fmt.Errorf("encode fields number: %w", err)
	}

	if err = ProtoUserName.EncodeMsgpack(enc); err != nil {
		return err
	}

	if err = enc.String(a.user); err != nil {
		return fmt.Errorf("encode username: %w", err)
	}

	if err = ProtoTuple.EncodeMsgpack(enc); err != nil {
		return err
	}

	if err = enc.ArrayLen(authTupleSize); err != nil {
		return fmt.Errorf("encode auth tuple size: %w", err)
	}

	if err = enc.String(authMethod); err != nil {
		return fmt.Errorf("encode auth method: %w", err)
	}

	if err = enc.Bytes(a.scramble([]byte(a.password), salt[:n])); err != nil {
		return fmt.Errorf("encode scrambled password: %w", err)
	}

	return nil
}

// Code implements Query interface.
func (*Auth) Code() Code {
	return CodeAuth
}

func (a *Auth) scramble(password, salt []byte) []byte {
	h := sha1.New()
	_, _ = h.Write(password)
	password = h.Sum(nil)

	h.Reset()
	_, _ = h.Write(password)
	tmp := h.Sum(nil)

	h.Reset()
	_, _ = h.Write(salt[:sha1.Size])
	_, _ = h.Write(tmp)

	return a.xor(password, h.Sum(nil), sha1.Size)
}

func (*Auth) xor(left, right []byte, size int) []byte {
	result := make([]byte, size)
	for i := 0; i < size; i++ {
		result[i] = left[i] ^ right[i]
	}

	return result
}
