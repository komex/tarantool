package tarantool

import (
	"fmt"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/komex/msgpack/v2"
)

// ExtUUID is a code of UUID extension.
const ExtUUID msgpack.Ext = 2

// UUID represents wrapper for uuid.UUID with implemented msgpack.CustomDecoder and msgpack.CustomEncoder interfaces.
type UUID uuid.UUID

// DecodeMsgpack implements msgpack.CustomDecoder interface.
func (u *UUID) DecodeMsgpack(dec *msgpack.Decoder) error {
	ext, data, err := dec.ExtData()
	if err != nil {
		return fmt.Errorf("decode extension data: %w", err)
	}

	if ext != ExtUUID {
		return ErrUnexpectedExtension
	}

	var v uuid.UUID

	if v, err = uuid.FromBytes(data); err != nil {
		return fmt.Errorf("convert from bytes: %w", err)
	}

	*u = UUID(v)

	return nil
}

// EncodeMsgpack implements msgpack.CustomEncoder interface.
func (u UUID) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := enc.ExtData(ExtUUID, u[:]); err != nil {
		return fmt.Errorf("encode UUID: %w", err)
	}

	return nil
}
