package tarantool_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/komex/msgpack/v2"

	"gitlab.com/komex/tarantool/v2"
)

const (
	rows = 1000000
)

func BenchmarkNewSelect(b *testing.B) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*3)
	b.Cleanup(cancel)

	require.NoError(b, prepareTestData(ctx, rows))

	handlers := []struct {
		name    string
		handler func(context.Context, tarantool.Driver, *tarantool.Select) error
	}{
		{
			"skip result",
			SkipResultHandler,
		},
		{
			"decode interface",
			DecodeInterfaceHandler,
		},
		{
			"decode struct",
			DecodeStructHandler,
		},
	}

	for _, slots := range []int{1, 2} {
		for _, parallelism := range []int{1, 10, 60, 80, 100, 120, 140} {
			b.Run(fmt.Sprintf("x%d-%d", slots, parallelism), func(b *testing.B) {
				for _, h := range handlers {
					b.Run(h.name, func(b *testing.B) {
						conn, err := getAuthConnection(
							ctx,
							address,
							tarantool.DefaultSlotsSize*slots,
							tarantool.DefaultBufferSize,
						)
						require.NoError(b, err)

						b.Cleanup(func() {
							require.NoError(b, conn.Close())
						})

						space, err := tarantool.GetSpace(ctx, conn, "tester")
						require.NoError(b, err)

						s := tarantool.NewSelect(space, nil, 1)
						s.Index, err = tarantool.GetIndex(ctx, conn, space, "primary")
						require.NoError(b, err)

						b.SetParallelism(parallelism)
						b.ReportAllocs()
						b.ResetTimer()
						b.RunParallel(func(pb *testing.PB) {
							for pb.Next() {
								require.NoError(b, h.handler(ctx, conn, s))
							}
						})
					})
				}
			})
		}
	}
}

func SkipResultHandler(ctx context.Context, conn tarantool.Driver, s *tarantool.Select) error {
	return conn.Exec(ctx, s)
}

func DecodeInterfaceHandler(ctx context.Context, conn tarantool.Driver, s *tarantool.Select) error {
	fn := msgpack.CustomDecoderFunc(func(dec *msgpack.Decoder) error {
		_, err := dec.Interface()

		return err
	})

	return conn.Query(ctx, s, tarantool.NewSingleResult(fn))
}

func DecodeStructHandler(ctx context.Context, conn tarantool.Driver, s *tarantool.Select) error {
	var result Row

	return conn.Query(ctx, s, tarantool.NewSingleResult(&result))
}
