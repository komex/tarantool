package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

// Spaces define a list of Space.
type Spaces []Space

// DecodeMsgpack implements interface msgpack.CustomDecoder.
func (s *Spaces) DecodeMsgpack(dec *msgpack.Decoder) error {
	n, err := dec.ArrayLen()
	if err != nil {
		return fmt.Errorf("decode number of spaces: %w", err)
	}

	*s = make(Spaces, n)

	for n = range *s {
		if err = (*s)[n].DecodeMsgpack(dec); err != nil {
			return fmt.Errorf("decode space: %w", err)
		}
	}

	return nil
}

// ByID returns a space by its id.
func (s Spaces) ByID(id uint64) *Space {
	for _, v := range s {
		if v.ID == id {
			return &v
		}
	}

	return nil
}

// ByName returns a space by its name.
func (s Spaces) ByName(name string) *Space {
	for _, v := range s {
		if v.Name == name {
			return &v
		}
	}

	return nil
}
