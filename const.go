package tarantool

const (
	saltSize   = 44
	maxSchemas = 10000
)

const (
	authMethod    = "chap-sha1"
	authKeysNum   = 2
	authTupleSize = 2
)

const (
	upsertQueryLen       = 3
	callQueryLen         = 2
	headerRequiredFields = 2
)

const (
	vSpaceSpaceID = 281
	vIndexSpaceID = 289
)
