package main

import (
	"context"
	"fmt"
	"net"
	"time"

	"gitlab.com/komex/msgpack/v2"

	"gitlab.com/komex/tarantool/v2"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	var conn tarantool.Driver
	{
		c, err := net.Dial("tcp", ":3301")
		if err != nil {
			panic(err)
		}
		conn, err = tarantool.NewConnection(c)
		if err != nil {
			_ = c.Close()
			panic(err)
		}
	}
	defer conn.Close()

	// Use your tarantool credentials
	if err := conn.Auth(ctx, "admin", "passwd"); err != nil {
		panic(err)
	}

	space, err := tarantool.GetSpace(ctx, conn, "tester")
	if err != nil {
		panic(err)
	}

	if space == nil {
		if err = createScheme(ctx, conn); err != nil {
			panic(err)
		}

		space, err = tarantool.GetSpace(ctx, conn, "tester")
		if err != nil {
			panic(err)
		}

		// Insert three tuples (our name for records) into the space
		if err = conn.Exec(ctx, tarantool.NewInsert(space, []any{1, "Roxette", 1986})); err != nil {
			panic(err)
		}
		if err = conn.Exec(ctx, tarantool.NewInsert(space, []any{2, "Scorpions", 2015})); err != nil {
			panic(err)
		}
		if err = conn.Exec(ctx, tarantool.NewInsert(space, []any{3, "Ace of Base", 1993})); err != nil {
			panic(err)
		}
	}

	// Select a tuple using the primary index
	var result any
	err = conn.Query(
		ctx,
		tarantool.NewSelect(space, tarantool.Key{3}, 1),
		tarantool.NewSingleResult(msgpack.CustomDecoderFunc(func(dec *msgpack.Decoder) error {
			result, err = dec.Interface()
			return err
		})),
	)
	if err != nil {
		panic(err)
	}

	fmt.Println(result)
}

func createScheme(ctx context.Context, conn tarantool.Driver) error {
	// unsupported Lua type 'function'
	_ = conn.Exec(ctx, tarantool.NewCall("box.schema.space.create", "tester"))

	// Format the created space by specifying field names and types
	err := conn.Exec(ctx, tarantool.NewCall(
		"box.space.tester:format",
		[]map[string]string{
			{
				"name": "id",
				"type": "unsigned",
			},
			{
				"name": "band_name",
				"type": "string",
			},
			{
				"name": "year",
				"type": "unsigned",
			},
		},
	))
	if err != nil {
		return err
	}

	// Create the first index (named primary)
	err = conn.Exec(ctx, tarantool.NewCall(
		"box.space.tester:create_index",
		"primary",
		map[string]any{
			"type":  "hash",
			"parts": []string{"id"},
		},
	))

	return err
}
