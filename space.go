package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

// Space defines Tarantool space object.
type Space struct {
	ID          uint64
	Owner       uint64
	Name        string
	Engine      string
	FieldsCount uint64
}

// EncodeMsgpack implements interface msgpack.CustomEncoder.
func (s *Space) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := enc.Uint8(uint8(ProtoSpaceNo)); err != nil {
		return fmt.Errorf("encode proto code: %w", err)
	}

	if err := enc.Uint64(s.ID); err != nil {
		return fmt.Errorf("encode space ID: %w", err)
	}

	return nil
}

// DecodeMsgpack implements interface msgpack.CustomDecoder.
func (s *Space) DecodeMsgpack(dec *msgpack.Decoder) error {
	const fieldsNumber = 5

	m, err := dec.ArrayLen()
	if err != nil {
		return fmt.Errorf("decode number of fields in space: %w", err)
	}

	if m < fieldsNumber {
		return ErrInvalidPackageSize
	}

	if s.ID, err = dec.Uint64(); err != nil {
		return fmt.Errorf("decode Space ID: %w", err)
	}

	if s.Owner, err = dec.Uint64(); err != nil {
		return fmt.Errorf("decode Space Owner: %w", err)
	}

	if s.Name, err = dec.String(); err != nil {
		return fmt.Errorf("decode Space Name: %w", err)
	}

	if s.Engine, err = dec.String(); err != nil {
		return fmt.Errorf("decode Space Engine: %w", err)
	}

	if s.FieldsCount, err = dec.Uint64(); err != nil {
		return fmt.Errorf("decode Space fields count: %w", err)
	}

	if err = Skip(dec, m-fieldsNumber); err != nil {
		return fmt.Errorf("skip unused fields: %w", err)
	}

	return nil
}
