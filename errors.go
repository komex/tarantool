package tarantool

import (
	"errors"
	"fmt"
)

var (
	ErrUnexpectedGreeting      = errors.New("unexpected tarantool server greeting message")
	ErrUnexpectedServerVersion = errors.New("unexpected tarantool server version")
	ErrNoConnectionSpecified   = errors.New("no connection was specified")
	ErrInvalidSaltSize         = errors.New("invalid salt size")
	ErrInvalidPackageSize      = errors.New("invalid package size")
	ErrTooMachResults          = errors.New("received too mach results")
	ErrGateInvalidState        = errors.New("gate has invalid state")
	ErrNotFound                = errors.New("was not found")
	ErrExchangeNotFound        = errors.New("exchange was not found")
	ErrSpaceNotFound           = errors.New("space was not found")
	ErrIndexNotFound           = errors.New("index was not found")
	ErrConnectionClosed        = errors.New("connection closed")
	ErrUnexpectedExtension     = errors.New("unexpected extension code")
)

// QueryError is a wrapper for failed Query.
type QueryError struct {
	Query Query
	Err   error
}

// NewQueryError creates a new instance of QueryError.
func NewQueryError(query Query, err error) *QueryError {
	return &QueryError{Query: query, Err: err}
}

// Unwrap is useful for unwrapping origin error.
func (q QueryError) Unwrap() error {
	return q.Err
}

// Cause is useful for unwrapping origin error.
func (q QueryError) Cause() error {
	return q.Err
}

// Error implements error interface.
func (q QueryError) Error() string {
	return fmt.Sprintf("command %s failed: %s", q.Query.Code(), q.Err)
}
