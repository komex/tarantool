package tarantool

import (
	"context"

	"gitlab.com/komex/msgpack/v2"
)

// Driver is an interface for communicates with Tarantool Server.
type Driver interface {
	// Auth authenticates user by password and reload available spaces and indexes.
	Auth(ctx context.Context, user, password string) error
	// Spaces returns a list of presented on a server spaces available to a user.
	Spaces(ctx context.Context) (Spaces, error)
	// Indexes returns a list of presented on a server indexes available to a user.
	Indexes(ctx context.Context) (Indexes, error)
	// Exec executes a query and do not returns result.
	Exec(ctx context.Context, query Query) error
	// Query executes a query and decodes response to result.
	Query(ctx context.Context, query Query, result msgpack.CustomDecoder) error
	// Close closes connection.
	Close() error
}
