module gitlab.com/komex/tarantool/v2

go 1.19

require (
	github.com/mailru/easygo v0.0.0-20190618140210-3c14a0dc985f
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.8.1
	gitlab.com/komex/msgpack/v2 v2.0.0
	golang.org/x/sync v0.1.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.3.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
