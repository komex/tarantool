package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

//go:generate stringer -type=Code -linecomment

const (
	CodeOK      Code = 0x00 // OK
	CodeSelect  Code = 0x01 // Select
	CodeInsert  Code = 0x02 // Insert
	CodeReplace Code = 0x03 // Replace
	CodeUpdate  Code = 0x04 // Update
	CodeDelete  Code = 0x05 // Delete
	CodeAuth    Code = 0x07 // Auth
	CodeEval    Code = 0x08 // Eval
	CodeUpsert  Code = 0x09 // Upsert
	CodeCall    Code = 0x0a // Call
	CodeExecute Code = 0x0b // Execute
	CodePrepare Code = 0x0d // Prepare
	CodePing    Code = 0x40 // Ping
)

// Code represents query type code.
type Code uint8

// EncodeMsgpack implements msgpack.CustomEncoder interface.
func (i Code) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := enc.Uint8(uint8(i)); err != nil {
		return fmt.Errorf("encode Code: %w", err)
	}

	return nil
}

// DecodeMsgpack implements msgpack.CustomDecoder interface.
func (i *Code) DecodeMsgpack(dec *msgpack.Decoder) error {
	code, err := dec.Uint64()
	if err != nil {
		return fmt.Errorf("decode Code: %w", err)
	}

	*i = Code(code)

	return nil
}
