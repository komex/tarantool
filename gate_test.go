package tarantool_test

import (
	"context"
	"errors"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/komex/tarantool/v2"
)

func TestGate_Wait(t *testing.T) {
	t.Parallel()

	testErr := errors.New("test error")

	tests := []struct {
		name            string
		canceledContext bool
		done            error
		doneAfter       time.Duration
		expectedError   error
		stateBeforeFree tarantool.GateState
		stateAfterFree  tarantool.GateState
	}{
		{
			"context",
			true,
			nil,
			time.Millisecond * 3,
			context.Canceled,
			tarantool.GateStateFailed,
			tarantool.GateStateFailed,
		},
		{
			"success done",
			false,
			nil,
			time.Nanosecond,
			nil,
			tarantool.GateStateDone,
			tarantool.GateStateIdle,
		},
		{
			"done with error",
			false,
			testErr,
			time.Nanosecond,
			testErr,
			tarantool.GateStateDone,
			tarantool.GateStateIdle,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			g, err := tarantool.NewGate(tarantool.CodeSelect, nil)
			require.NoError(t, err)
			require.True(t, g.Ready())

			var wg sync.WaitGroup
			wg.Add(1)

			go func() {
				<-time.After(test.doneAfter)
				g.Done(test.done)
				wg.Done()
			}()

			ctx, cancel := context.WithCancel(context.Background())
			t.Cleanup(cancel)
			if test.canceledContext {
				cancel()
			}

			require.ErrorIs(t, g.Wait(ctx), test.expectedError)
			require.Equal(t, g.State(), test.stateBeforeFree)

			g.Free()

			require.Equal(t, g.State(), test.stateAfterFree)

			wg.Wait()
		})
	}
}
