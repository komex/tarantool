package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

// Upsert defines Tarantool upsert query.
type Upsert struct {
	Space      *Space
	Data       any
	Operations Operations
}

// NewUpsert creates a new instance of Upsert.
func NewUpsert(space *Space, data any, operations Operations) *Upsert {
	return &Upsert{Space: space, Data: data, Operations: operations}
}

// EncodeMsgpack implements interface Query.
func (u *Upsert) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := enc.MapLen(upsertQueryLen); err != nil {
		return fmt.Errorf("encode fields number: %w", err)
	}

	if err := u.Space.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode space: %w", err)
	}

	if err := ProtoTuple.EncodeMsgpack(enc); err != nil {
		return err
	}

	if err := enc.Interface(u.Data); err != nil {
		return fmt.Errorf("encode data: %w", err)
	}

	if err := ProtoDefTuple.EncodeMsgpack(enc); err != nil {
		return err
	}

	if err := u.Operations.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode operations: %w", err)
	}

	return nil
}

// Code implements interface Query.
func (*Upsert) Code() Code {
	return CodeUpsert
}
