package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

const insertQueryLen = 2

// Insert defines Tarantool insert query.
type Insert struct {
	Space   *Space
	Data    any
	Replace bool
}

// NewInsert creates a new instance of Insert.
func NewInsert(space *Space, data any) *Insert {
	return &Insert{Space: space, Data: data}
}

// NewReplace creates a new instance of Insert with replace flag.
func NewReplace(space *Space, data any) *Insert {
	return &Insert{Space: space, Replace: true, Data: data}
}

// EncodeMsgpack implements interface msgpack.CustomEncoder.
func (i *Insert) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := enc.MapLen(insertQueryLen); err != nil {
		return fmt.Errorf("encode fields number: %w", err)
	}

	if err := i.Space.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode space: %w", err)
	}

	if err := ProtoTuple.EncodeMsgpack(enc); err != nil {
		return err
	}

	if err := enc.Interface(i.Data); err != nil {
		return fmt.Errorf("encode data: %w", err)
	}

	return nil
}

// Code implements interface Query.
func (i *Insert) Code() Code {
	if i.Replace {
		return CodeReplace
	}

	return CodeInsert
}
