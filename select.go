package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

// Select defines Tarantool select query.
type Select struct {
	Space    *Space
	Index    *Index
	Iterator Iterator
	Key      Key
	Limit    uint64
	Offset   uint64
}

// NewSelect creates a new instance of Select.
func NewSelect(space *Space, key Key, limit uint64) *Select {
	return &Select{Space: space, Key: key, Limit: limit}
}

// EncodeMsgpack implements interface msgpack.CustomEncoder.
func (s *Select) EncodeMsgpack(enc *msgpack.Encoder) error {
	const (
		minFields           = 3
		maxAdditionalFields = 3
	)

	additionalEncoders := make([]func(*msgpack.Encoder) error, 0, maxAdditionalFields)

	if s.Iterator > 0 {
		additionalEncoders = append(additionalEncoders, s.encodeIterator)
	}

	if s.Offset > 0 {
		additionalEncoders = append(additionalEncoders, s.encodeOffset)
	}

	if s.Index != nil {
		additionalEncoders = append(additionalEncoders, s.Index.EncodeMsgpack)
	}

	if err := enc.MapLen(minFields + len(additionalEncoders)); err != nil {
		return fmt.Errorf("encode map length: %w", err)
	}
	// Required fields
	if err := s.Space.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode space ID: %w", err)
	}

	if err := ProtoLimit.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode limit proto code: %w", err)
	}

	if err := enc.Uint64(s.Limit); err != nil {
		return fmt.Errorf("encode limit: %w", err)
	}

	if err := s.Key.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode key: %w", err)
	}

	for _, f := range additionalEncoders {
		if err := f(enc); err != nil {
			return fmt.Errorf("encode additional field: %w", err)
		}
	}

	return nil
}

// Code implements interface Query.
func (*Select) Code() Code {
	return CodeSelect
}

func (s *Select) encodeOffset(enc *msgpack.Encoder) error {
	if err := ProtoOffset.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode offset key: %w", err)
	}

	if err := enc.Uint64(s.Offset); err != nil {
		return fmt.Errorf("encode offset value: %w", err)
	}

	return nil
}

func (s *Select) encodeIterator(enc *msgpack.Encoder) error {
	if err := ProtoIterator.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode iterator key: %w", err)
	}

	if err := enc.Uint8(uint8(s.Iterator)); err != nil {
		return fmt.Errorf("encode iterator value: %w", err)
	}

	return nil
}
