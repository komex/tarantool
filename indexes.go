package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

// Indexes define a list of Index.
type Indexes []Index

// DecodeMsgpack implements interface msgpack.CustomDecoder.
func (i *Indexes) DecodeMsgpack(dec *msgpack.Decoder) error {
	n, err := dec.ArrayLen()
	if err != nil {
		return fmt.Errorf("decode number of indexes: %w", err)
	}

	*i = make(Indexes, n)

	for n = range *i {
		if err = (*i)[n].DecodeMsgpack(dec); err != nil {
			return fmt.Errorf("decode index: %w", err)
		}
	}

	return nil
}

// ByID returns an index by its id.
func (i Indexes) ByID(space *Space, id uint64) *Index {
	if space == nil {
		return nil
	}

	for _, v := range i {
		if v.SpaceID != space.ID {
			continue
		}

		if v.ID == id {
			return &v
		}
	}

	return nil
}

// ByName returns an index by its name.
func (i Indexes) ByName(space *Space, name string) *Index {
	if space == nil {
		return nil
	}

	for _, v := range i {
		if v.SpaceID != space.ID {
			continue
		}

		if v.Name == name {
			return &v
		}
	}

	return nil
}
