package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

// Key defines query key.
type Key []any

// EncodeMsgpack implements interface msgpack.CustomEncoder.
func (k Key) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := ProtoKey.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode proto key: %w", err)
	}

	if err := enc.ArrayLen(len(k)); err != nil {
		return fmt.Errorf("encode elements length: %w", err)
	}

	for _, v := range k {
		if err := enc.Interface(v); err != nil {
			return fmt.Errorf("encode element: %w", err)
		}
	}

	return nil
}
