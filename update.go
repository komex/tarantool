package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

const updateQueryLen = 4

// Update defines Tarantool update query.
type Update struct {
	Space      *Space
	Index      *Index
	Key        Key
	Operations Operations
}

// NewUpdate creates a new instance of Update.
func NewUpdate(space *Space, index *Index, key Key, operations Operations) *Update {
	return &Update{Space: space, Index: index, Key: key, Operations: operations}
}

// EncodeMsgpack implements interface Query.
func (u *Update) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := enc.MapLen(updateQueryLen); err != nil {
		return fmt.Errorf("encode fields number: %w", err)
	}

	if err := u.Space.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode space: %w", err)
	}

	if err := u.Index.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode index: %w", err)
	}

	if err := u.Key.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode key: %w", err)
	}

	if err := ProtoTuple.EncodeMsgpack(enc); err != nil {
		return err
	}

	if err := u.Operations.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode operations: %w", err)
	}

	return nil
}

// Code implements interface Query.
func (*Update) Code() Code {
	return CodeUpdate
}
