package tarantool

import (
	"gitlab.com/komex/msgpack/v2"
)

// Query defines interface for all types of tarantool queries.
type Query interface {
	msgpack.CustomEncoder
	// Code returns Code of query.
	Code() Code
}
