package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

// Call provides tarantool call/eval Query.
type Call struct {
	code       Code
	key        Proto
	expression string
	arguments  []any
}

// NewCall returns a new call Query.
func NewCall(function string, arguments ...any) *Call {
	return &Call{code: CodeCall, key: ProtoFunctionName, expression: function, arguments: arguments}
}

// NewEval returns a new eval Query.
func NewEval(expression string, arguments ...any) *Call {
	return &Call{code: CodeEval, key: ProtoExpression, expression: expression, arguments: arguments}
}

// EncodeMsgpack implements msgpack.CustomEncoder interface.
func (c *Call) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := enc.MapLen(callQueryLen); err != nil {
		return fmt.Errorf("encode fields number: %w", err)
	}

	if err := c.key.EncodeMsgpack(enc); err != nil {
		return err
	}

	if err := enc.String(c.expression); err != nil {
		return fmt.Errorf("encode expression: %w", err)
	}

	if err := ProtoTuple.EncodeMsgpack(enc); err != nil {
		return err
	}

	if err := enc.ArrayLen(len(c.arguments)); err != nil {
		return fmt.Errorf("encode arguments number: %w", err)
	}

	for _, arg := range c.arguments {
		if err := enc.Interface(arg); err != nil {
			return fmt.Errorf("encode argument: %w", err)
		}
	}

	return nil
}

// Code implements Query interface.
func (c *Call) Code() Code {
	return c.code
}
