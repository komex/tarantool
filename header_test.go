package tarantool_test

import (
	"bytes"
	"errors"
	"reflect"
	"testing"

	"gitlab.com/komex/msgpack/v2"

	"gitlab.com/komex/tarantool/v2"
)

func TestHeader_EncodeMsgpack(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name     string
		header   tarantool.Header
		expected map[tarantool.Proto]uint8
	}{
		{
			"full struct",
			tarantool.Header{
				Code:    tarantool.CodeInsert,
				Sync:    123,
				Scheme:  45,
				HasData: true,
				Error:   errors.New("some test error"),
			},
			map[tarantool.Proto]uint8{
				tarantool.ProtoCode:   uint8(tarantool.CodeInsert),
				tarantool.ProtoSync:   123,
				tarantool.ProtoSchema: 45,
			},
		},
		{
			"empty struct",
			tarantool.Header{},
			map[tarantool.Proto]uint8{
				tarantool.ProtoCode: 0,
				tarantool.ProtoSync: 0,
			},
		},
		{
			"no scheme",
			tarantool.Header{
				Code:    tarantool.CodeInsert,
				Sync:    123,
				HasData: true,
				Error:   errors.New("some test error"),
			},
			map[tarantool.Proto]uint8{
				tarantool.ProtoCode: uint8(tarantool.CodeInsert),
				tarantool.ProtoSync: 123,
			},
		},
		{
			"no size",
			tarantool.Header{
				Code:    tarantool.CodeInsert,
				Sync:    123,
				Scheme:  45,
				HasData: true,
				Error:   errors.New("some test error"),
			},
			map[tarantool.Proto]uint8{
				tarantool.ProtoCode:   uint8(tarantool.CodeInsert),
				tarantool.ProtoSync:   123,
				tarantool.ProtoSchema: 45,
			},
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			var buf bytes.Buffer
			enc := msgpack.NewEncoder(&buf)
			dec := msgpack.NewDecoder(&buf)

			if err := test.header.EncodeMsgpack(enc); err != nil {
				t.Fatal(err)
			}

			v, err := dec.Interface()
			if err != nil {
				t.Fatal(err)
			}

			expected := make(map[any]any, len(test.expected))
			for k, v := range test.expected {
				expected[uint8(k)] = v
			}

			if !reflect.DeepEqual(v, expected) {
				t.Errorf("EncodeMsgpack() decoded = %v, want %v", v, expected)
			}
		})
	}
}

func TestHeader_Reset(t *testing.T) {
	t.Parallel()

	h := tarantool.Header{
		Code:    tarantool.CodeInsert,
		Sync:    123,
		Scheme:  45,
		HasData: true,
		Error:   errors.New("some test error"),
	}

	h.Reset()

	var emptyHeader tarantool.Header
	if h != emptyHeader {
		t.Fatal("failed to reset header state")
	}
}

func TestHeader_DecodeMsgpack(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name     string
		decoder  func() *msgpack.Decoder
		expected tarantool.Header
	}{
		{
			"with full header",
			func() *msgpack.Decoder {
				var buf bytes.Buffer
				enc := msgpack.NewEncoder(&buf)
				// encode header
				_ = enc.Interface(map[tarantool.Proto]uint64{
					tarantool.ProtoSync:   123,
					tarantool.ProtoCode:   uint64(tarantool.CodeSelect),
					tarantool.ProtoSchema: 567,
				})
				// skip body
				_ = enc.MapLen(0)

				return msgpack.NewDecoder(&buf)
			},
			tarantool.Header{
				Code:   tarantool.CodeSelect,
				Sync:   123,
				Scheme: 567,
			},
		},
		{
			"with partial header",
			func() *msgpack.Decoder {
				var buf bytes.Buffer
				enc := msgpack.NewEncoder(&buf)
				// encode header
				_ = enc.Interface(map[tarantool.Proto]uint64{
					tarantool.ProtoSync: 123,
					tarantool.ProtoCode: uint64(tarantool.CodeOK),
				})
				// skip body
				_ = enc.MapLen(0)

				return msgpack.NewDecoder(&buf)
			},
			tarantool.Header{
				Sync: 123,
			},
		},
		{
			"with content",
			func() *msgpack.Decoder {
				var buf bytes.Buffer
				enc := msgpack.NewEncoder(&buf)
				// skip header
				_ = enc.MapLen(0)
				// encode body
				_ = enc.Interface(map[tarantool.Proto]uint64{
					tarantool.ProtoData: 123,
				})

				return msgpack.NewDecoder(&buf)
			},
			tarantool.Header{
				HasData: true,
			},
		},
		{
			"with error",
			func() *msgpack.Decoder {
				var buf bytes.Buffer
				enc := msgpack.NewEncoder(&buf)
				// skip header
				_ = enc.MapLen(0)
				// encode body
				_ = enc.Interface(map[tarantool.Proto]string{
					tarantool.ProtoError24: "test error",
				})

				return msgpack.NewDecoder(&buf)
			},
			tarantool.Header{
				Error: errors.New("test error"),
			},
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			var h tarantool.Header
			if err := h.DecodeMsgpack(test.decoder()); err != nil {
				t.Fatal(err)
			}

			// Check errors
			if h.Error != nil && test.expected.Error != nil {
				if h.Error.Error() != test.expected.Error.Error() {
					t.Errorf("EncodeMsgpack() encoded = %v, want %v", h, test.expected)
				}
				// Errors are equals. Reset them
				h.Error, test.expected.Error = nil, nil
			}

			if h != test.expected {
				t.Errorf("EncodeMsgpack() encoded = %v, want %v", h, test.expected)
			}
		})
	}
}
