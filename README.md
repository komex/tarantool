# Client in Go for Tarantool 2.0+

[![pipeline status](https://gitlab.com/komex/tarantool/badges/master/pipeline.svg)](https://gitlab.com/komex/tarantool/commits/master)
[![coverage report](https://gitlab.com/komex/tarantool/badges/master/coverage.svg)](https://gitlab.com/komex/tarantool/commits/master)

The `tarantool` package has everything necessary for interfacing with
[Tarantool 2.0+](http://tarantool.org/).

## Table of contents

* [Installation](#installation)
* [Example](#example)
* [Help](#help)

## Installation

We assume that you have Tarantool version 2.0 and a modern Linux or BSD operating system.

To download and install, say:

```
$ go get gitlab.com/komex/tarantool/v2
```

## Example

We can now have a closer look at the [`example.go`](examples/example.go) program and make some observations about what
it does.

```go
package main

import (
	"context"
	"fmt"
	"net"
	"time"

	"gitlab.com/komex/tarantool/v2"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	var conn *tarantool.Connection
	{
		c, err := net.Dial("tcp", ":3301")
		if err != nil {
			panic(err)
		}
		conn, err = tarantool.NewConnection(c)
		if err != nil {
			_ = c.Close()
			panic(err)
		}
	}
	defer conn.Close()

	// Use your tarantool credentials
	if err := conn.Auth(ctx, "admin", "passwd"); err != nil {
		panic(err)
	}

	spaces, err := conn.Spaces(ctx)
	if err != nil {
		panic(err)
	}

	space := spaces.ByName("tester")

	if space == nil {
		if err := createScheme(ctx, conn); err != nil {
			panic(err)
		}

		// Reload system info
		spaces, err = conn.Spaces(ctx)
		if err != nil {
			panic(err)
		}
		space = spaces.ByName("tester")

		// Insert three tuples (our name for records) into the space
		if err := conn.Exec(ctx, tarantool.NewInsert(space, []interface{}{1, "Roxette", 1986})); err != nil {
			panic(err)
		}
		if err := conn.Exec(ctx, tarantool.NewInsert(space, []interface{}{2, "Scorpions", 2015})); err != nil {
			panic(err)
		}
		if err := conn.Exec(ctx, tarantool.NewInsert(space, []interface{}{3, "Ace of Base", 1993})); err != nil {
			panic(err)
		}
	}

	// Select a tuple using the primary index
	single := tarantool.NewSingleResult(nil)
	if err := conn.Query(ctx, tarantool.NewSelect(space, tarantool.Key{3}, 1), single); err != nil {
		panic(err)
	}

	fmt.Println(single.Result)
}

func createScheme(ctx context.Context, conn *tarantool.Connection) error {
	// unsupported Lua type 'function'
	_ = conn.Exec(ctx, tarantool.NewCall("box.schema.space.create", "tester"))

	// Format the created space by specifying field names and types
	err := conn.Exec(ctx, tarantool.NewCall(
		"box.space.tester:format",
		[]map[string]string{
			{
				"name": "id",
				"type": "unsigned",
			},
			{
				"name": "band_name",
				"type": "string",
			},
			{
				"name": "year",
				"type": "unsigned",
			},
		},
	))
	if err != nil {
		return err
	}

	// Create the first index (named primary)
	err = conn.Exec(ctx, tarantool.NewCall(
		"box.space.tester:create_index",
		"primary",
		map[string]interface{}{
			"type":  "hash",
			"parts": []string{"id"},
		},
	))

	return err
}
```

## Help

To contact `go-tarantool` developers on any problems, create an issue at
[komex/tarantool](https://gitlab.com/komex/tarantool/issues).
