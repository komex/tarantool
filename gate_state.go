package tarantool

//go:generate stringer -type=GateState -output=gate_state_string.go -trimprefix=GateState
type GateState uint8

const (
	GateStateIdle GateState = iota
	GateStateReady
	GateStateDone
	GateStateFailed
)
