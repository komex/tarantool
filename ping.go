package tarantool

import (
	"gitlab.com/komex/msgpack/v2"
)

// Ping defines Tarantool Ping query.
type Ping struct{}

// NewPing creates a new instance of Ping.
func NewPing() *Ping {
	return new(Ping)
}

// EncodeMsgpack implements interface msgpack.CustomEncoder.
func (Ping) EncodeMsgpack(*msgpack.Encoder) error {
	return nil
}

// Code implements interface Query.
func (Ping) Code() Code {
	return CodePing
}
