package tarantool

import (
	"bytes"
	"fmt"
	"io"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/komex/msgpack/v2"
)

// Decoder is a structure for decoding response packet.
type Decoder struct {
	buf      bytes.Buffer
	header   Header
	dec      *msgpack.Decoder
	exchange *Exchange
}

// NewDecoder returns a new instance of Decoder.
func NewDecoder(exchange *Exchange) *Decoder {
	d := Decoder{exchange: exchange}
	d.dec = msgpack.NewDecoder(&d.buf)

	d.dec.RegisterExtDecoder(ExtUUID, func(data []byte) (any, error) {
		v, err := uuid.FromBytes(data)
		if err != nil {
			return nil, fmt.Errorf("convert from bytes: %w", err)
		}

		return v, nil
	})

	return &d
}

// Fill resets and fills buffer from r with n bytes.
func (d *Decoder) Fill(r io.Reader, n uint64) error {
	d.buf.Reset()

	if _, err := io.CopyN(&d.buf, r, int64(n)); err != nil {
		return fmt.Errorf("copy packet: %w", err)
	}

	d.header.Reset()

	return nil
}

// Packet decodes packet from buffer filled with Fill method.
func (d *Decoder) Packet() error {
	if err := d.header.DecodeMsgpack(d.dec); err != nil {
		return fmt.Errorf("decode header: %w", err)
	}

	g := d.exchange.Pull(d.header.Sync)
	if g == nil {
		return fmt.Errorf("unknown Sync code: %w", ErrExchangeNotFound)
	}

	// If response has error or has no data
	//nolint:nilerr // Method must return only processing data errors.
	if !d.header.HasData || d.header.Error != nil {
		g.Done(d.header.Error)

		return nil
	}

	// Response has data, but we do not need it
	if g.result == nil {
		d.header.Error = d.dec.Skip()
		if d.header.Error != nil {
			d.header.Error = fmt.Errorf("skip package body: %w", d.header.Error)
		}

		g.Done(d.header.Error)

		return d.header.Error
	}

	// Decode message
	d.header.Error = g.result.DecodeMsgpack(d.dec)
	if d.header.Error != nil {
		d.header.Error = fmt.Errorf("decode result data: %w", d.header.Error)
	}

	g.Done(d.header.Error)

	return nil
}
