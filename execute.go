package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

const executeQueryLen = 3

// Execute defines Tarantool execute query.
type Execute struct {
	query     string
	arguments []any
}

// NewExecute creates a new instance of Execute query.
func NewExecute(query string, arguments ...any) *Execute {
	return &Execute{query: query, arguments: arguments}
}

// EncodeMsgpack implements interface msgpack.CustomEncoder.
func (e *Execute) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := enc.MapLen(executeQueryLen); err != nil {
		return fmt.Errorf("encode fields number: %w", err)
	}

	if err := ProtoSQLText.EncodeMsgpack(enc); err != nil {
		return err
	}

	if err := enc.String(e.query); err != nil {
		return fmt.Errorf("encode query: %w", err)
	}

	if err := ProtoSQLBind.EncodeMsgpack(enc); err != nil {
		return err
	}

	if err := enc.ArrayLen(len(e.arguments)); err != nil {
		return fmt.Errorf("encode arguments number: %w", err)
	}

	for i := range e.arguments {
		if err := enc.Interface(e.arguments[i]); err != nil {
			return fmt.Errorf("encode sql bind argument: %w", err)
		}
	}

	if err := ProtoOptions.EncodeMsgpack(enc); err != nil {
		return err
	}

	if err := enc.ArrayLen(0); err != nil {
		return fmt.Errorf("encode options number: %w", err)
	}

	return nil
}

// Code implements interface Query.
func (*Execute) Code() Code {
	return CodeExecute
}
