package tarantool

import (
	"sync"
)

type Exchange struct {
	list map[uint64]*Gate
	mu   sync.Mutex
}

// NewExchange returns a new instance of Exchange.
func NewExchange(size int) *Exchange {
	return &Exchange{list: make(map[uint64]*Gate, size)}
}

func (e *Exchange) Pull(id uint64) *Gate {
	e.mu.Lock()
	g := e.list[id]
	delete(e.list, id)
	e.mu.Unlock()

	return g
}

func (e *Exchange) Push(gate *Gate) {
	if gate == nil {
		return
	}

	e.mu.Lock()
	e.list[gate.ID()] = gate
	e.mu.Unlock()
}

func (e *Exchange) Clear(err error) {
	e.mu.Lock()
	for id, gate := range e.list {
		gate.Done(err)
		delete(e.list, id)
	}
	e.mu.Unlock()
}
