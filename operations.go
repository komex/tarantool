package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

// Operations supports fast encoding slice of Operation in msgpack format.
type Operations []Operation

// EncodeMsgpack implements msgpack.CustomEncoder interface.
func (o Operations) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := enc.ArrayLen(len(o)); err != nil {
		return fmt.Errorf("encode operations count: %w", err)
	}

	for i := range o {
		if err := o[i].EncodeMsgpack(enc); err != nil {
			return fmt.Errorf("encode operation: %w", err)
		}
	}

	return nil
}
