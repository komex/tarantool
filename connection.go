package tarantool

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io"
	"net"
	"sync"
	"sync/atomic"
	"time"

	"github.com/mailru/easygo/netpoll"
	"gitlab.com/komex/msgpack/v2"
)

// Connection allows to establish connection and communicates with Tarantool server.
type Connection struct {
	ProtocolVersion string
	SessionID       string

	conn     net.Conn
	r        *bufio.Reader
	w        *bufio.Writer
	e        *msgpack.Encoder
	d        *msgpack.Decoder
	slots    chan *Gate
	decoders chan *Decoder
	exchange *Exchange
	queries  int32
	queue    int32
	closer   sync.Once
	closed   chan struct{}
	salt     []byte
}

// NewConnection returns a new instance of Connection.
func NewConnection(conn net.Conn, options ...Option) (*Connection, error) {
	if conn == nil {
		return nil, ErrNoConnectionSpecified
	}

	opts := DefaultOptions()
	for _, apply := range options {
		apply(&opts)
	}

	c := Connection{
		conn:     conn,
		r:        bufio.NewReaderSize(conn, opts.BufferSize.Read),
		w:        bufio.NewWriterSize(conn, opts.BufferSize.Write),
		exchange: NewExchange(opts.SlotsSize),
		slots:    make(chan *Gate, opts.SlotsSize),
		decoders: make(chan *Decoder, opts.SlotsSize),
		closed:   make(chan struct{}),
	}
	c.e = msgpack.NewEncoder(c.w)
	c.d = msgpack.NewDecoder(c.r)

	if err := c.handshake(); err != nil {
		return nil, fmt.Errorf("handshake: %w", err)
	}

	if err := c.polling(); err != nil {
		return nil, fmt.Errorf("polling: %w", err)
	}

	for i := 0; i < opts.SlotsSize; i++ {
		c.decoders <- NewDecoder(c.exchange)
	}

	go c.requests()

	return &c, nil
}

// Spaces returns a list of presented on a server spaces available to a user.
func (c *Connection) Spaces(ctx context.Context) (Spaces, error) {
	var spaces Spaces
	if err := c.Query(ctx, NewSelect(&Space{ID: vSpaceSpaceID}, nil, maxSchemas), &spaces); err != nil {
		return nil, fmt.Errorf("load spaces: %w", err)
	}

	return spaces, nil
}

// Indexes returns a list of presented on a server indexes available to a user.
func (c *Connection) Indexes(ctx context.Context) (Indexes, error) {
	var indexes Indexes
	if err := c.Query(ctx, NewSelect(&Space{ID: vIndexSpaceID}, nil, maxSchemas), &indexes); err != nil {
		return nil, fmt.Errorf("load indexes: %w", err)
	}

	return indexes, nil
}

// Exec executes a query and do not returns result.
func (c *Connection) Exec(ctx context.Context, query Query) error {
	return c.Query(ctx, query, nil)
}

// Query executes a query and decodes response to result.
func (c *Connection) Query(ctx context.Context, query Query, result msgpack.CustomDecoder) error {
	// Register a new query
	atomic.AddInt32(&c.queries, 1)
	defer atomic.AddInt32(&c.queries, -1)

	// Check if connection is already closed.
	select {
	case <-c.closed:
		return ErrConnectionClosed
	default:
	}

	// Prepare gate to be sent to server
	gate, err := NewGate(query.Code(), result)
	if err != nil {
		return fmt.Errorf("create gate: %w", NewQueryError(query, err))
	}

	defer gate.Free()

	if err = query.EncodeMsgpack(gate.Encoder); err != nil {
		return fmt.Errorf("encode command: %w", NewQueryError(query, err))
	}

	// Trying to sent gate using free slot
	select {
	case c.slots <- gate:
		err = gate.Wait(ctx)
	case <-ctx.Done():
		err = fmt.Errorf("no free slots: %w", ctx.Err())
	}

	if err != nil {
		return NewQueryError(query, err)
	}

	return nil
}

// Close closes connection.
func (c *Connection) Close() error {
	err := ErrConnectionClosed

	c.closer.Do(func() {
		// Stop fetching new requests
		close(c.closed)

		// If there are some queries in progress, we shall wait they are all done before closing slots channel
		// Check it every millisecond and stop ticker at the end
		ticker := time.NewTicker(time.Millisecond)
		for atomic.LoadInt32(&c.queries) > 0 {
			<-ticker.C
		}
		ticker.Stop()
		close(c.slots)

		// Remove all available decoders from queue
		// Without decoders, responses will not be processed
		for i := 0; i < cap(c.decoders); i++ {
			<-c.decoders
		}
		close(c.decoders)

		// Closing connection to server
		if err = c.conn.Close(); err != nil {
			err = fmt.Errorf("close connection: %w", err)
		}
	})

	return err
}

// Auth authenticates user by password and reload available spaces and indexes.
func (c *Connection) Auth(ctx context.Context, user, password string) error {
	q := NewAuth(user, password)
	q.salt = c.salt

	if err := c.Exec(ctx, q); err != nil {
		return fmt.Errorf("authenticate: %w", err)
	}

	return nil
}

func (c *Connection) requests() {
	for gate := range c.slots {
		c.pushGate(gate)
		// If queue contains more requests we will process it in one batch
		for l := len(c.slots); l > 0; l-- {
			c.pushGate(<-c.slots)
		}

		if err := c.w.Flush(); err != nil {
			c.exchange.Clear(fmt.Errorf("flush gates: %w", err))
		}
	}
}

func (c *Connection) pushGate(gate *Gate) {
	// Do nothing with empty gate
	if gate == nil {
		panic(fmt.Errorf("nil pointer: %w", ErrGateInvalidState))
	}

	if !gate.Ready() {
		// Gate has Failed state only if context was done. Just ignore this gate
		if gate.State() == GateStateFailed {
			return
		}

		panic(fmt.Errorf("gate is %s with error %v: %w", gate.State(), gate.err, ErrGateInvalidState))
	}

	c.exchange.Push(gate)
	atomic.AddInt32(&c.queue, 1)

	if err := c.write(gate); err != nil {
		// In case of fail to write we need to decrement queue counter and remove gate from exchange
		atomic.AddInt32(&c.queue, -1)
		_ = c.exchange.Pull(gate.ID())
		gate.Done(fmt.Errorf("write gate: %w", err))
	}
}

func (c *Connection) responses(_ netpoll.Event) {
	for {
		// Lock-free check responses in queue
		l := atomic.LoadInt32(&c.queue)
		if l <= 0 {
			return
		}

		if !atomic.CompareAndSwapInt32(&c.queue, l, l-1) {
			continue
		}

		dec := <-c.decoders
		// Do not handle response if decoders channel is closed
		if dec == nil {
			return
		}

		size, err := c.d.Uint64()
		if err != nil {
			c.decoders <- dec
			c.exchange.Clear(fmt.Errorf("decode package size: %w", err))

			return
		}

		if err = dec.Fill(c.r, size); err != nil {
			c.decoders <- dec
			c.exchange.Clear(fmt.Errorf("fill decoder: %w", err))

			return
		}

		go func() {
			// TODO: log packet error
			_ = dec.Packet()
			c.decoders <- dec
		}()
	}
}

func (c *Connection) write(exchange *Gate) error {
	if err := c.e.Uint64(uint64(exchange.Len())); err != nil {
		return fmt.Errorf("encode package length: %w", err)
	}

	if _, err := exchange.WriteTo(c.w); err != nil {
		return fmt.Errorf("send package: %w", err)
	}

	return nil
}

func (c *Connection) polling() error {
	poller, err := netpoll.New(nil)
	if err != nil {
		return fmt.Errorf("create poller: %w", err)
	}

	desc, err := netpoll.HandleRead(c.conn)
	if err != nil {
		return fmt.Errorf("create read descriptor: %w", err)
	}

	if err = poller.Start(desc, c.responses); err != nil {
		return fmt.Errorf("start polling: %w", err)
	}

	return nil
}

func (c *Connection) handshake() error {
	const (
		GreetingPrefix       = "Tarantool"
		GreetingPrefixLength = len(GreetingPrefix) + 1
		GreetingLineMaxSize  = 64
		GreetingLineMinSize  = 17
		GreetingSize         = GreetingLineMaxSize * 2 // greeting package size (contains two lines)
	)

	greeting := make([]byte, GreetingSize)
	if _, err := io.ReadFull(c.r, greeting); err != nil {
		return fmt.Errorf("read greeting message: %w", err)
	}

	// Check that the response is really from the Tarantool server.
	if !bytes.HasPrefix(greeting, []byte(GreetingPrefix)) {
		return fmt.Errorf("unknown protocol name: %w", ErrUnexpectedGreeting)
	}

	// Extract protocol version and session ID from first line
	{
		line := bytes.TrimSpace(greeting[:GreetingLineMaxSize])
		if len(line) < GreetingLineMinSize {
			return fmt.Errorf("invalid first line length: %w", ErrUnexpectedGreeting)
		}

		i := bytes.LastIndexByte(line, ' ')
		if i < 0 {
			return fmt.Errorf("invalid first line content: %w", ErrUnexpectedGreeting)
		}
		version, session := line[GreetingPrefixLength:i], line[i+1:]

		if !(len(version) >= 3 && version[0] == '2' && version[1] == '.') {
			return ErrUnexpectedServerVersion
		}

		c.ProtocolVersion = string(version)
		c.SessionID = string(session)
	}

	// Copy salt
	line := bytes.TrimSpace(greeting[GreetingLineMaxSize:])
	if len(line) < saltSize {
		return ErrInvalidSaltSize
	}

	c.salt = make([]byte, len(line))
	copy(c.salt, line)

	return nil
}
