package tarantool

type Iterator uint8

const (
	IterateEqual              Iterator = iota // key == x ASC order
	IterateReverseEqual                       // key == x DESC order
	IterateAll                                // all tuples
	IterateLessThan                           // key < x
	IterateLessThanOrEqual                    // key <= x
	IterateGreaterThan                        // key >= x
	IterateGreaterThanOrEqual                 // key > x
	IterateBitsAllSet                         // all bits from x are set in key
	IterateBitsAnySet                         // at least one x's bit is set
	IterateBitsAllNotSet                      // all bits are not set
)
