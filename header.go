package tarantool

import (
	"errors"
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

// Header represents Tarantool binary protocol message header.
type Header struct {
	Error   error
	Sync    uint64
	Scheme  uint64
	Code    Code
	HasData bool
}

// Reset resets header values to default.
func (h *Header) Reset() {
	*h = Header{}
}

// EncodeMsgpack implements interface msgpack.CustomEncoder.
func (h *Header) EncodeMsgpack(enc *msgpack.Encoder) error {
	size := headerRequiredFields
	if h.Scheme > 0 {
		size++
	}

	if err := enc.MapLen(size); err != nil {
		return fmt.Errorf("encode header size: %w", err)
	}

	if err := ProtoCode.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode header key: %w", err)
	}

	if err := h.Code.EncodeMsgpack(enc); err != nil {
		return err
	}

	if err := ProtoSync.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode header key: %w", err)
	}

	if err := enc.Uint64(h.Sync); err != nil {
		return fmt.Errorf("encode header value: %w", err)
	}

	if h.Scheme > 0 {
		if err := ProtoSchema.EncodeMsgpack(enc); err != nil {
			return fmt.Errorf("encode header key: %w", err)
		}

		if err := enc.Uint64(h.Scheme); err != nil {
			return fmt.Errorf("encode header value: %w", err)
		}
	}

	return nil
}

// DecodeMsgpack implements interface msgpack.CustomDecoder.
func (h *Header) DecodeMsgpack(dec *msgpack.Decoder) error {
	if err := h.decodeHeader(dec); err != nil {
		return fmt.Errorf("decode header: %w", err)
	}

	if err := h.decodeBody(dec); err != nil {
		return fmt.Errorf("decode body: %w", err)
	}

	return nil
}

func (*Header) decodeStruct(dec *msgpack.Decoder, switcher func(Proto, *msgpack.Decoder) error) error {
	elements, err := dec.MapLen()
	if err != nil {
		return fmt.Errorf("decode struct size: %w", err)
	}

	for ; elements > 0; elements-- {
		var p Proto

		if err = p.DecodeMsgpack(dec); err != nil {
			return fmt.Errorf("decode struct code: %w", err)
		}

		if err = switcher(p, dec); err != nil {
			return err
		}
	}

	return nil
}

func (h *Header) decodeHeader(dec *msgpack.Decoder) error {
	return h.decodeStruct(dec, func(code Proto, dec *msgpack.Decoder) error {
		switch code {
		case ProtoCode:
			if err := h.Code.DecodeMsgpack(dec); err != nil {
				return fmt.Errorf("decode header code value: %w", err)
			}
		case ProtoSync:
			var err error
			if h.Sync, err = dec.Uint64(); err != nil {
				return fmt.Errorf("decode header sync value: %w", err)
			}
		case ProtoSchema:
			var err error
			if h.Scheme, err = dec.Uint64(); err != nil {
				return fmt.Errorf("decode header scheme: %w", err)
			}
		default:
			if err := dec.Skip(); err != nil {
				return fmt.Errorf("skip unknown header code: %w", err)
			}
		}

		return nil
	})
}

func (h *Header) decodeBody(dec *msgpack.Decoder) error {
	return h.decodeStruct(dec, func(code Proto, dec *msgpack.Decoder) error {
		switch code {
		case ProtoData:
			h.HasData = true
		case ProtoError24:
			msg, err := dec.String()
			if err != nil {
				return fmt.Errorf("decode error message: %w", err)
			}

			h.Error = errors.New(msg)
		case ProtoError:
			var e Error
			if err := e.DecodeMsgpack(dec); err != nil {
				return fmt.Errorf("decode error message: %w", err)
			}

			if e.Message != "" {
				h.Error = &e
			}
		default:
			if err := dec.Skip(); err != nil {
				return fmt.Errorf("skip unknown header code: %w", err)
			}
		}

		return nil
	})
}
