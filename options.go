package tarantool

const (
	// Default size for the maximum number of requests sent at a time.
	DefaultSlotsSize = 255
	// Default size of connection buffer.
	DefaultBufferSize = 4096
)

// Options defines full list of supported options.
type Options struct {
	// BufferSize allows to configure size of buffers.
	BufferSize struct {
		// Read defines a size of read buffer.
		Read int
		// Write defines a size of write buffer.
		Write int
	}
	// SlotsSize represents maximum number of slots.
	SlotsSize int
}

// Option defines interface for configure Options.
type Option func(*Options)

// DefaultOptions creates Options with default values.
func DefaultOptions() Options {
	var o Options
	o.BufferSize.Read = DefaultBufferSize
	o.BufferSize.Write = DefaultBufferSize
	o.SlotsSize = DefaultSlotsSize

	return o
}

// BufferSize creates Option allows to set read and write buffer sizes.
func BufferSize(size int) Option {
	return func(o *Options) {
		o.BufferSize.Read = size
		o.BufferSize.Write = size
	}
}

// ReadBufferSize creates Option allows to set read buffer size.
func ReadBufferSize(size int) Option {
	return func(o *Options) {
		o.BufferSize.Read = size
	}
}

// WriteBufferSize creates Option allows to set write buffer size.
func WriteBufferSize(size int) Option {
	return func(o *Options) {
		o.BufferSize.Write = size
	}
}

// SlotSize creates Option allows to set maximum number of slots.
func SlotSize(size int) Option {
	return func(o *Options) {
		o.SlotsSize = size
	}
}
