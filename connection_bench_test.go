package tarantool_test

import (
	"context"
	"net"
	"testing"
	"time"

	"gitlab.com/komex/msgpack/v2"

	"gitlab.com/komex/tarantool/v2"
)

func BenchmarkConnection_Query(b *testing.B) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*3)
	b.Cleanup(cancel)

	var conn *tarantool.Connection
	{
		c, err := net.Dial("tcp", ":3301")
		if err != nil {
			panic(err)
		}
		conn, err = tarantool.NewConnection(c)
		if err != nil {
			_ = c.Close()
			b.Fatal(err)
		}
	}
	b.Cleanup(func() {
		_ = conn.Close()
	})

	// Use your tarantool credentials
	if err := conn.Auth(ctx, "admin", "passwd"); err != nil {
		b.Fatal(err)
	}

	space, err := tarantool.GetSpace(ctx, conn, "tester")
	if err != nil {
		b.Fatal(err)
	}

	// Select a tuple using the primary index
	s := tarantool.NewSelect(space, tarantool.Key{3}, 1)

	benchmarks := []struct {
		name        string
		parallelism int
	}{
		{
			"1",
			1,
		},
		{
			"2",
			2,
		},
		{
			"5",
			5,
		},
		{
			"10",
			10,
		},
		{
			"100",
			100,
		},
		{
			"255",
			255,
		},
		{
			"1000",
			1000,
		},
	}
	skip := msgpack.CustomDecoderFunc(func(dec *msgpack.Decoder) error {
		return dec.Skip()
	})

	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			b.ReportAllocs()
			b.SetParallelism(bm.parallelism)
			b.RunParallel(func(pb *testing.PB) {
				single := tarantool.NewSingleResult(skip)
				for pb.Next() {
					if err := conn.Query(ctx, s, single); err != nil {
						b.Fatal(err)
					}
				}
			})
		})
	}
}
