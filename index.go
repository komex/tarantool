package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

// Index defines Tarantool index object.
type Index struct {
	SpaceID uint64
	ID      uint64
	Name    string
	Type    string
}

// EncodeMsgpack implements interface msgpack.CustomEncoder.
func (i *Index) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := ProtoIndexNo.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode index key: %w", err)
	}

	if err := enc.Uint64(i.ID); err != nil {
		return fmt.Errorf("encode index ID: %w", err)
	}

	return nil
}

// DecodeMsgpack implements interface msgpack.CustomDecoder.
func (i *Index) DecodeMsgpack(dec *msgpack.Decoder) error {
	const fieldsNumber = 4

	m, err := dec.ArrayLen()
	if err != nil {
		return fmt.Errorf("decode index package size: %w", err)
	}

	if m < fieldsNumber {
		return ErrInvalidPackageSize
	}

	if i.SpaceID, err = dec.Uint64(); err != nil {
		return fmt.Errorf("decode Index Space ID: %w", err)
	}

	if i.ID, err = dec.Uint64(); err != nil {
		return fmt.Errorf("decode Index ID: %w", err)
	}

	if i.Name, err = dec.String(); err != nil {
		return fmt.Errorf("decode Index Name: %w", err)
	}

	if i.Type, err = dec.String(); err != nil {
		return fmt.Errorf("decode Index Type: %w", err)
	}

	if err = Skip(dec, m-fieldsNumber); err != nil {
		return fmt.Errorf("skip unused fields: %w", err)
	}

	return nil
}
