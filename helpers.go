package tarantool

import (
	"context"
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

const singleResult = 1

// SingleResult helps to decode single response to Result.
type SingleResult struct {
	dec msgpack.CustomDecoder
}

// NewSingleResult returns a new instance of SingleResult.
func NewSingleResult(dec msgpack.CustomDecoder) *SingleResult {
	return &SingleResult{dec: dec}
}

// DecodeMsgpack implements interface msgpack.CustomDecoder.
func (s *SingleResult) DecodeMsgpack(dec *msgpack.Decoder) error {
	elements, err := dec.ArrayLen()
	if err != nil {
		return fmt.Errorf("decode result length: %w", err)
	}

	if elements == 0 {
		return ErrNotFound
	}

	if elements > singleResult {
		return ErrTooMachResults
	}

	if err = s.dec.DecodeMsgpack(dec); err != nil {
		return fmt.Errorf("decode response: %w", err)
	}

	return nil
}

// GetSpace finds a space by its name. Method returns ErrSpaceNotFound if space was not found.
func GetSpace(ctx context.Context, conn Driver, name string) (*Space, error) {
	if conn == nil {
		return nil, ErrNoConnectionSpecified
	}

	spaces, err := conn.Spaces(ctx)
	if err != nil {
		return nil, fmt.Errorf("fetch spaces: %w", err)
	}

	space := spaces.ByName(name)
	if space == nil {
		return nil, ErrSpaceNotFound
	}

	return space, nil
}

// GetIndex finds an index by its name in specified space. Method returns ErrIndexNotFound if index was not found.
func GetIndex(ctx context.Context, conn Driver, space *Space, name string) (*Index, error) {
	if conn == nil {
		return nil, ErrNoConnectionSpecified
	}

	indexes, err := conn.Indexes(ctx)
	if err != nil {
		return nil, fmt.Errorf("fetch indexes: %w", err)
	}

	index := indexes.ByName(space, name)
	if index == nil {
		return nil, ErrIndexNotFound
	}

	return index, nil
}

// Skip skips decoding of num elements.
func Skip(dec *msgpack.Decoder, num int) error {
	for i := 0; i < num; i++ {
		if err := dec.Skip(); err != nil {
			return fmt.Errorf("skip element: %w", err)
		}
	}

	return nil
}
