package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

// ErrorCode represents error codes from Tarantool Server.
type ErrorCode uint8

const (
	ErrorType    ErrorCode = 0x00
	ErrorFile    ErrorCode = 0x01
	ErrorLine    ErrorCode = 0x02
	ErrorMessage ErrorCode = 0x03
	ErrorErrNo   ErrorCode = 0x04
	ErrorErrCode ErrorCode = 0x05
	ErrorFields  ErrorCode = 0x06
)

// Error is a struct for error response from Tarantool Server.
type Error struct {
	Type    string
	File    string
	Line    uint32
	Message string
	ErrNo   uint16
	ErrCode uint16
	Fields  map[string]any
}

// DecodeMsgpack implements msgpack.CustomDecoder interface.
func (e *Error) DecodeMsgpack(dec *msgpack.Decoder) error {
	l, err := dec.MapLen()
	if err != nil {
		return fmt.Errorf("decode error map length: %w", err)
	}

	var code uint8

	for ; l > 0; l-- {
		if code, err = dec.Uint8(); err != nil {
			return fmt.Errorf("decode error field code: %w", err)
		}

		switch ErrorCode(code) {
		case ErrorType:
			err = dec.Skip()
			// TODO decode error type after Tarantool fix response
			// e.Type, err = dec.String()
		case ErrorFile:
			e.File, err = dec.String()
		case ErrorLine:
			e.Line, err = dec.Uint32()
		case ErrorMessage:
			e.Message, err = dec.String()
		case ErrorErrNo:
			e.ErrNo, err = dec.Uint16()
		case ErrorErrCode:
			e.ErrCode, err = dec.Uint16()
		case ErrorFields:
			err = e.decodeFields(dec)
		}

		if err != nil {
			return fmt.Errorf("decode error field value: %w", err)
		}
	}

	return nil
}

// Error implements error interface.
func (e *Error) Error() string {
	return e.Message
}

func (e *Error) decodeFields(dec *msgpack.Decoder) error {
	l, err := dec.MapLen()
	if err != nil {
		return fmt.Errorf("decode error fields length: %w", err)
	}

	if l == 0 {
		return nil
	}

	e.Fields = make(map[string]any, l)

	var k string

	for ; l > 0; l-- {
		if k, err = dec.String(); err != nil {
			return fmt.Errorf("decode additional field key: %w", err)
		}

		if e.Fields[k], err = dec.Interface(); err != nil {
			return fmt.Errorf("decode additional field value: %w", err)
		}
	}

	return nil
}
