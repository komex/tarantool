package tarantool_test

import (
	"bytes"
	"reflect"
	"testing"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/komex/msgpack/v2"

	"gitlab.com/komex/tarantool/v2"
)

func TestUUID_EncodeMsgpack(t *testing.T) {
	t.Parallel()

	id, err := uuid.FromString("5f85a430-aa82-4f55-879f-989b3c49d3cf")
	if err != nil {
		t.Fatal(err)
	}

	var b bytes.Buffer
	if err = (tarantool.UUID)(id).EncodeMsgpack(msgpack.NewEncoder(&b)); err != nil {
		t.Fatal(err)
	}
	// Expected: header + data
	// Header: fixed size 16 + UUID code
	if !reflect.DeepEqual(append([]byte{216, 2}, id.Bytes()...), b.Bytes()) {
		t.Fatal("unexpected result")
	}
}

func TestUUID_DecodeMsgpack(t *testing.T) {
	t.Parallel()

	id, err := uuid.FromString("5f85a430-aa82-4f55-879f-989b3c49d3cf")
	if err != nil {
		t.Fatal(err)
	}

	var (
		b      bytes.Buffer
		result uuid.UUID
	)
	// Header: fixed size 16 + UUID code
	_, _ = b.Write(append([]byte{216, 2}, id.Bytes()...))

	if err = (*tarantool.UUID)(&result).DecodeMsgpack(msgpack.NewDecoder(&b)); err != nil {
		t.Fatal(err)
	}

	if !uuid.Equal(id, result) {
		t.Fatal("unexpected result")
	}
}
