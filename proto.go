package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

//go:generate stringer -type=Proto -linecomment

const (
	ProtoCode         Proto = 0x00 // Code
	ProtoSync         Proto = 0x01 // Sync
	ProtoSchema       Proto = 0x05 // Schema
	ProtoSpaceNo      Proto = 0x10 // SpaceNo
	ProtoIndexNo      Proto = 0x11 // IndexNo
	ProtoLimit        Proto = 0x12 // Limit
	ProtoOffset       Proto = 0x13 // Offset
	ProtoIterator     Proto = 0x14 // Iterator
	ProtoKey          Proto = 0x20 // Key
	ProtoTuple        Proto = 0x21 // Tuple
	ProtoFunctionName Proto = 0x22 // FunctionName
	ProtoUserName     Proto = 0x23 // UserName
	ProtoExpression   Proto = 0x27 // Expression
	ProtoDefTuple     Proto = 0x28 // DefTuple
	ProtoData         Proto = 0x30 // Data
	ProtoError24      Proto = 0x31 // Error24
	ProtoMetaData     Proto = 0x32 // MetaData
	ProtoSQLBindMeta  Proto = 0x33 // SQLBindMeta
	ProtoSQLBindCount Proto = 0x34 // SQLBindCount
	ProtoSQLText      Proto = 0x40 // SQLText
	ProtoSQLBind      Proto = 0x41 // SQLBind
	ProtoSQLInfo      Proto = 0x42 // SQLInfo
	ProtoSQLStatement Proto = 0x43 // SQLStatement
	ProtoOptions      Proto = 0x2b // Options
	ProtoError        Proto = 0x52 // Error
)

// Proto defines reserved codes in tarantool binary protocol.
type Proto uint8

// EncodeMsgpack implements interface msgpack.CustomEncoder.
func (i Proto) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := enc.Uint8(uint8(i)); err != nil {
		return fmt.Errorf("encode Proto: %w", err)
	}

	return nil
}

// DecodeMsgpack implements interface msgpack.CustomDecoder.
func (i *Proto) DecodeMsgpack(dec *msgpack.Decoder) error {
	p, err := dec.Uint8()
	if err != nil {
		return fmt.Errorf("decode Proto: %w", err)
	}

	*i = Proto(p)

	return nil
}
