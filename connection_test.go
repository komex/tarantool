package tarantool_test

import (
	"context"
	"errors"
	"fmt"
	"net"
	"os"
	"sync/atomic"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/komex/msgpack/v2"
	"golang.org/x/sync/errgroup"

	"gitlab.com/komex/tarantool/v2"
)

const (
	address = ":3301"
	login   = "admin"
	pass    = "passwd"
)

func TestConnection_Close(t *testing.T) {
	t.Parallel()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
	defer cancel()

	conn, err := getConnection(address, tarantool.DefaultSlotsSize, tarantool.DefaultBufferSize)
	require.NoError(t, err)

	_, err = conn.Indexes(ctx)
	require.NoError(t, err)

	err = conn.Close()
	require.NoError(t, err)

	err = conn.Close()
	require.ErrorIs(t, err, tarantool.ErrConnectionClosed)

	_, err = conn.Indexes(ctx)
	require.ErrorIs(t, err, tarantool.ErrConnectionClosed)
}

func TestConnection_Meta(t *testing.T) {
	t.Parallel()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
	t.Cleanup(cancel)

	conn, err := getConnection(address, tarantool.DefaultSlotsSize, tarantool.DefaultBufferSize)
	require.NoError(t, err)

	t.Cleanup(func() {
		require.NoError(t, conn.Close())
	})

	t.Run("spaces", func(t *testing.T) {
		t.Parallel()

		spaces, err := conn.Spaces(ctx)
		require.NoError(t, err)
		require.NotZero(t, len(spaces))
	})

	t.Run("indexes", func(t *testing.T) {
		t.Parallel()

		indexes, err := conn.Indexes(ctx)
		require.NoError(t, err)
		require.NotZero(t, len(indexes))
	})
}

func TestConnection_Auth(t *testing.T) {
	t.Parallel()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
	t.Cleanup(cancel)

	conn, err := getConnection(address, tarantool.DefaultSlotsSize, tarantool.DefaultBufferSize)
	require.NoError(t, err)

	t.Cleanup(func() {
		require.NoError(t, conn.Close())
	})

	tests := []struct {
		name     string
		login    string
		password string
		expected bool
	}{
		{
			"valid",
			login,
			pass,
			false,
		},
		{
			"invalid login",
			login + login,
			pass,
			true,
		},
		{
			"invalid password",
			login,
			pass + pass,
			true,
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			err := conn.Auth(ctx, test.login, test.password)
			if test.expected {
				require.ErrorContains(t, err, "User not found or supplied credentials are invalid")
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func getConnection(address string, slotsSize, bufferSize int) (*tarantool.Connection, error) {
	c, err := net.Dial("tcp", os.Getenv("TARANTOOL_HOST")+address)
	if err != nil {
		return nil, fmt.Errorf("connection to tarantool server failed: %w", err)
	}

	conn, err := tarantool.NewConnection(c, tarantool.SlotSize(slotsSize), tarantool.BufferSize(bufferSize))
	if err != nil {
		_ = c.Close()

		return nil, err
	}

	return conn, nil
}

func getAuthConnection(ctx context.Context, address string, slotsSize, bufferSize int) (*tarantool.Connection, error) {
	conn, err := getConnection(address, slotsSize, bufferSize)
	if err != nil {
		return nil, err
	}

	if err = conn.Auth(ctx, login, pass); err != nil {
		return nil, err
	}

	return conn, nil
}

func prepareTestData(ctx context.Context, rows uint64) error {
	conn, err := getAuthConnection(ctx, address, tarantool.DefaultSlotsSize, tarantool.DefaultBufferSize)
	if err != nil {
		return err
	}
	defer conn.Close()

	var result uint64

	err = conn.Query(
		ctx,
		tarantool.NewCall("box.space.tester:count"),
		tarantool.NewSingleResult(msgpack.CustomDecoderFunc(func(dec *msgpack.Decoder) error {
			result, err = dec.Uint64()

			return err
		})),
	)
	if err != nil {
		if errors.Unwrap(err).Error() != "Procedure 'box.space.test:count' is not defined" {
			return err
		}

		if err = createScheme(ctx, conn); err != nil {
			return fmt.Errorf("create scheme: %w", err)
		}
	}

	if result != rows {
		err = conn.Exec(ctx, tarantool.NewCall("box.space.tester:truncate"))
		if err != nil {
			return err
		}

		err = generateTestData(ctx, conn, rows)
		if err != nil {
			return err
		}
	}

	return nil
}

func createScheme(ctx context.Context, conn tarantool.Driver) error {
	_ = conn.Exec(ctx, tarantool.NewCall("box.schema.space.create", "tester"))

	err := conn.Exec(ctx, tarantool.NewCall(
		"box.space.tester:format",
		[]map[string]string{
			{"name": "id", "type": "unsigned"},
			{"name": "text", "type": "string"},
			{"name": "f", "type": "number"},
		},
	))
	if err != nil {
		return fmt.Errorf("format: %w", err)
	}

	err = conn.Exec(ctx, tarantool.NewCall(
		"box.space.tester:create_index",
		"primary",
		map[string]string{"type": "tree"},
	))
	if err != nil {
		return fmt.Errorf("create index: %w", err)
	}

	return err
}

func generateTestData(ctx context.Context, conn tarantool.Driver, rows uint64) error {
	space, err := tarantool.GetSpace(ctx, conn, "tester")
	if err != nil {
		return fmt.Errorf("fetch spaces: %w", err)
	}

	const threads = 100

	var counter uint64

	g, ctx := errgroup.WithContext(ctx)

	for i := 0; i < threads; i++ {
		g.Go(func() error {
			row := Row{
				text: "test string",
				f:    3.14,
				t:    time.Now(),
			}
			insert := tarantool.NewInsert(space, &row)
			for {
				row.id = atomic.AddUint64(&counter, 1)
				if row.id > rows {
					break
				}
				if err := conn.Exec(ctx, insert); err != nil {
					return err
				}
			}

			return nil
		})
	}

	return g.Wait()
}

type Row struct {
	id   uint64
	text string
	f    float32
	t    time.Time
}

func (r *Row) DecodeMsgpack(dec *msgpack.Decoder) error {
	n, err := dec.ArrayLen()
	if err != nil {
		return err
	}

	if n != 4 {
		return errors.New("bad format")
	}

	if r.id, err = dec.Uint64(); err != nil {
		return err
	}

	if r.text, err = dec.String(); err != nil {
		return err
	}

	if r.f, err = dec.Float32(); err != nil {
		return err
	}

	if r.t, err = dec.Time(); err != nil {
		return err
	}

	return nil
}

func (r *Row) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := enc.ArrayLen(4); err != nil {
		return err
	}

	if err := enc.Uint64(r.id); err != nil {
		return err
	}

	if err := enc.String(r.text); err != nil {
		return err
	}

	if err := enc.Float32(r.f); err != nil {
		return err
	}

	if err := enc.Time(r.t); err != nil {
		return err
	}

	return nil
}
