package tarantool

import (
	"bytes"
	"context"
	"fmt"
	"sync"

	"gitlab.com/komex/msgpack/v2"
)

var gatePool = sync.Pool{
	New: func() any {
		g := Gate{
			wait: make(chan bool, 1),
		}
		g.Encoder = msgpack.NewEncoder(&g.Buffer)

		return &g
	},
}

// Gate links query and response.
type Gate struct {
	header Header
	result msgpack.CustomDecoder
	wait   chan bool
	err    error
	mu     sync.Mutex
	state  GateState
	bytes.Buffer
	*msgpack.Encoder
}

// NewGate returns a new instance of Gate.
// Each Gate MUST be Free() after usage.
func NewGate(code Code, result msgpack.CustomDecoder) (*Gate, error) {
	e, ok := gatePool.Get().(*Gate)
	if !ok {
		panic("unexpected type in gate pool")
	}

	e.header.Code = code
	e.header.Sync = NextSyncSequenceValue()
	e.result = result

	if err := e.header.EncodeMsgpack(e.Encoder); err != nil {
		e.Free()

		return nil, fmt.Errorf("encode gate header: %w", err)
	}

	return e, nil
}

// ID returns an unique request-response ID.
func (g *Gate) ID() uint64 {
	return g.header.Sync
}

// Wait blocks goroutine until fetch response.
func (g *Gate) Wait(ctx context.Context) error {
	select {
	case <-g.wait:
		return g.err
	case <-ctx.Done():
		g.mu.Lock()
		g.state = GateStateFailed
		g.mu.Unlock()

		return fmt.Errorf("wait: %w", ctx.Err())
	}
}

// Free frees resources and returns Gate to pool.
// This gate can't be used after invoking this method.
func (g *Gate) Free() {
	if g == nil {
		return
	}

	g.mu.Lock()

	// Always reset Gate
	g.Buffer.Reset()
	g.header.Reset()
	g.result = nil

	// Do not move Gate in invalid state to pool
	if g.state == GateStateDone {
		g.state = GateStateIdle
		g.err = nil
		gatePool.Put(g)
	} else {
		close(g.wait)
	}

	g.mu.Unlock()
}

func (g *Gate) State() GateState {
	g.mu.Lock()
	defer g.mu.Unlock()

	return g.state
}

func (g *Gate) Done(err error) {
	g.mu.Lock()
	if g.state == GateStateReady {
		g.state = GateStateDone
		g.err = err
		g.wait <- true
	}
	g.mu.Unlock()
}

func (g *Gate) Ready() bool {
	var r bool

	g.mu.Lock()
	if g.state == GateStateIdle {
		g.state = GateStateReady
		r = true
	}
	g.mu.Unlock()

	return r
}
