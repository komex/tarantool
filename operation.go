package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

const operationQueryLen = 3

// Operation defines Tarantool operation.
type Operation struct {
	code     byte
	field    uint32
	argument any
}

// EncodeMsgpack implements interface msgpack.CustomEncoder.
func (o *Operation) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := enc.ArrayLen(operationQueryLen); err != nil {
		return fmt.Errorf("encode fields number: %w", err)
	}

	if err := enc.String(string(o.code)); err != nil {
		return fmt.Errorf("encode operation code: %w", err)
	}

	if err := enc.Uint32(o.field); err != nil {
		return fmt.Errorf("encode operation field number: %w", err)
	}

	if err := enc.Interface(o.argument); err != nil {
		return fmt.Errorf("encode operation argument: %w", err)
	}

	return nil
}

func SumOperation(field uint32, argument any) Operation {
	return Operation{code: '+', field: field, argument: argument}
}

func SubOperation(field uint32, argument any) Operation {
	return Operation{code: '-', field: field, argument: argument}
}

func BitAndOperation(field, argument uint32) Operation {
	return Operation{code: '&', field: field, argument: argument}
}

func BitOrOperation(field, argument uint32) Operation {
	return Operation{code: '|', field: field, argument: argument}
}

func BitXorOperation(field, argument uint32) Operation {
	return Operation{code: '^', field: field, argument: argument}
}

func RemoveOperation(field, num uint32) Operation {
	return Operation{code: '#', field: field, argument: num}
}

func InsertOperation(beforeField uint32, argument any) Operation {
	return Operation{code: '!', field: beforeField, argument: argument}
}

func SetOperation(field uint32, argument any) Operation {
	return Operation{code: '=', field: field, argument: argument}
}
