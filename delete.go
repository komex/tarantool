package tarantool

import (
	"fmt"

	"gitlab.com/komex/msgpack/v2"
)

const deleteQueryLen = 3

// Delete provides delete tuple Query.
type Delete struct {
	Space *Space
	Index *Index
	Key   Key
}

// NewDelete returns a new instance of delete tuple Query.
func NewDelete(space *Space, index *Index, key Key) *Delete {
	return &Delete{Space: space, Index: index, Key: key}
}

// EncodeMsgpack implements msgpack.CustomEncoder interface.
func (d *Delete) EncodeMsgpack(enc *msgpack.Encoder) error {
	if err := enc.MapLen(deleteQueryLen); err != nil {
		return fmt.Errorf("encode map length: %w", err)
	}

	if err := d.Space.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode space ID: %w", err)
	}

	if err := d.Index.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode index ID: %w", err)
	}

	if err := d.Key.EncodeMsgpack(enc); err != nil {
		return fmt.Errorf("encode key: %w", err)
	}

	return nil
}

// Code implements Query interface.
func (*Delete) Code() Code {
	return CodeDelete
}
