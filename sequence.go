package tarantool

import (
	"sync/atomic"
)

var syncSequence uint64

// NextSyncSequenceValue returns next unique sequence value.
func NextSyncSequenceValue() uint64 {
	return atomic.AddUint64(&syncSequence, 1)
}
